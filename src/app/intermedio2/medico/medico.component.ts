import { Component, OnInit } from '@angular/core';
import { MedicoService } from './medico.service';

@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styleUrls: []
})
export class MedicoComponent implements OnInit {

  // tslint:disable-next-line:variable-name
  constructor( public _medicoService: MedicoService ) { }

  medicos: any[] = [];

  ngOnInit() {
  }

  saludarMedico( nombre: string ) {
    return `Hola ${ nombre }`;
  }

  obtenerMedico() {
    this._medicoService.getMedicos()
      .subscribe( (medicos: any[]) => this.medicos = medicos );
  }

}
