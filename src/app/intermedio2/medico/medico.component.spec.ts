import { TestBed, ComponentFixture } from '@angular/core/testing';

import { MedicoComponent } from './medico.component';
import { MedicoService } from './medico.service';
import { HttpClientModule } from '@angular/common/http';


describe('Medico Component', () => {

    let component: MedicoComponent;
    let fixture: ComponentFixture<MedicoComponent>;

    beforeEach( () => {
        TestBed.configureTestingModule({
            declarations: [ MedicoComponent ],
            providers: [ MedicoService ],
            imports: [ HttpClientModule ]
        });

        // El TestBed regresa un Fixture
        fixture = TestBed.createComponent( MedicoComponent );
        component = fixture.componentInstance; // Obtener lo que regresa el fixtrure
    });


    it('Debe de crease el componente', () => {
        expect( component ).toBeTruthy();
    });

    it('Debe de retornar el nombre del médico', () => {
        expect( component ).toBeTruthy();

        const nombre = 'Juan';
        const res = component.saludarMedico( nombre );

        expect( res ).toContain( nombre );
    });

});
