import { TestBed, ComponentFixture } from '@angular/core/testing';
import { IncrementadorComponent } from './incrementador.component';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';


describe('Incremendator Component', () => {

    let component: IncrementadorComponent;
    let fixture: ComponentFixture<IncrementadorComponent>;

    beforeEach( () => {
        TestBed.configureTestingModule({
            declarations: [ IncrementadorComponent ],
            imports: [ FormsModule ]
        });

        fixture = TestBed.createComponent(IncrementadorComponent);
        component = fixture.componentInstance;

    });

    it('Debe de mostrar la leyenda', () => {

        component.leyenda = 'Progreso de carga';

        // Es necesario indicarle a angular que dispare la detección de cambios de la Leyenda
        fixture.detectChanges(); // Disparar la detección de cambios

        const elem: HTMLElement = fixture.debugElement.query( By.css('h3') ).nativeElement; // Regresa el primero que encuentra

        expect( elem.innerHTML ).toContain('Progreso de carga');

    });

    it('Debe de mostrar en el input el valor del progreso', () => {

        component.cambiarValor(5);
        fixture.detectChanges();

        // whenStable, permite crear una promesa para que
        // se se haga la prueba con los cambios que se necesitan aplicar
        fixture.whenStable().then( () => {
            const input = fixture.debugElement.query( By.css('input') );
            const elem: HTMLInputElement = input.nativeElement;

            console.log( elem );
            expect( elem.value ).toBe( '55' );
        });

    });

    it('Debe de incrementar/decrementar en 5, con un click en el botón', () => {
        const botones = fixture.debugElement.queryAll( By.css('.btn-primary ') );

        console.log( botones );

        botones[0].triggerEventHandler('click', null);
        expect( component.progreso ).toBe(45);

        botones[1].triggerEventHandler('click', null);
        expect( component.progreso ).toBe(50);

    });

    it('En el título del componente, debe mostrar valor del progreso', () => {


        // Llamar evento del botón qué disminye el valor en 5
        const botones = fixture.debugElement.queryAll( By.css('.btn-primary ') );
        botones[0].triggerEventHandler('click', null);
        fixture.detectChanges(); // Detectar los cambios

        // Identificar la variables progreso del lado del HTML
        const elem: HTMLElement = fixture.debugElement.query( By.css('h3') ).nativeElement;

        // Prueba: El resultado debe ser 45
        expect( elem.innerHTML ).toContain('45');
    });

});
