import { obtenerRobots } from './arreglos';

describe( 'Pruebas de arreglos', () => {

    it('Debe de retornar al menos 3 robots', () => {

        const res = obtenerRobots();
        // expect( res.length ).toBe(3); // Retorna 3 Robtos
        expect( res.length ).toBeGreaterThanOrEqual(3); // Retorna 3 o más

    });

    it('Debe de existir MegaMan y Ultron', () => {
        const res = obtenerRobots();
        // Si una de las dos validaciones falla,
        // este It de la prueba falla
        expect( res ).toContain('MegaMan');
        expect( res ).toContain('Ultron');
    });
});
