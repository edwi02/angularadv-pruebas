import { FormularioRegister } from './formularios';
import { FormArrayName, FormBuilder} from '@angular/forms';


describe('Formularios', () => {

    let componete: FormularioRegister;

    beforeEach( () => {

        componete = new FormularioRegister( new FormBuilder() );
    });

    it( 'Debe de crear un formulario con dos campos, email y password', () => {
        expect( componete.form.contains('email') ).toBeTruthy();
        expect( componete.form.contains('password') ).toBeTruthy();
    });

    // Validaciones sobre los campos
    it('El email debe de ser obligatorio', () => {

        const control = componete.form.get('email'); // campo
        control.setValue ('');
        expect( control.valid ).toBeFalsy();
    });

    // Validaciones sobre los campos
    it('El email debe de ser un correo valido', () => {

        const control = componete.form.get('email'); // campo
        control.setValue ('edwin@gmail.com');
        expect( control.valid ).toBeTruthy();
    });
});
