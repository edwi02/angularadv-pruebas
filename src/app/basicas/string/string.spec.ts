import { mensaje } from './string';


// Sirve para agrupar pruebas
// describe('Pruebas de Strings');

// Estas son las pruebas., esto es de Jarmine. Debe tener información clara.
// it('Debe de regresar un String');

describe('Pruebas de Strings', () => {
    // Primera pruebas
    it('Debe de regresar un String', () => {
        const respuesta = mensaje('Edwin Quintero');

        // expect( (typeof respuesta) === 'string' );
        expect(typeof respuesta).toBe('string');
    });

    // Segunda prueba, debe de retornar un saludo
    it('Debe de retornar un saludo con el nombre enviado', () => {
        const nombre = 'Juan';
        const resp = mensaje(nombre);
        // expect( resp ).toBe(`Saludos ${ nombre }`);
        expect( resp ).toContain( nombre );
    });
});

