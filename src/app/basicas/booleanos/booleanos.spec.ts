import { usuarioIngresado } from './booleanos';

describe('Pruebas de booleanos', () => {

    it('Debe de retornar TRUE', () => {
        const res = usuarioIngresado();

        // expect( res ).toBeTruthy(true);
        expect( res ).not.toBeTruthy();
    });
});
